﻿using System;

namespace Properties {

    public sealed class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve, done
        public string Seed
        { 
            get { return this.seed; }
        }

        // TODO improve, done
        public string Name
        {
            get { return this.name; }
        }

        // TODO improve, done
        public int Ordinal
        {
           get { return this.ordial; }
        }

        public override string ToString()
        {
           return ($"Name = {name}, Seed = {seed}, Ordinal = {ordial} ");
        }

        public override bool Equals(object obj)
        {
            //Done
            if(obj is Card) 
            {
                Card other = (Card) obj;
                return this.name == other.name
                    && this.seed == other.seed
                    && this.ordial == other.ordial;
            }
            else 
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            const int x = 31;
            return x * this.name.GetHashCode() 
                + x * x * this.seed.GetHashCode() 
                + x * x * x * this.ordial.GetHashCode();
        }
    }

}